/*
 * Main tasks
 * grunt build:styleguide - build to build folder
 * watch:assemble
 *
 * */

var r = require('ramda');

function App(grunt) {

    // Watch after file changes and fire necessary tasks
    grunt.config.merge({
        watch: {
            options: {
                spawn: false
            },
            templates: {
                files: ['<%= pkg.src %>/**/*.html'],
                tasks: ['copy:templates']
            },
            root: {
                files: ['<%= pkg.src %>/*.html'],
                tasks: ['copy:root']
            }
        }
    });

    grunt.config.merge({
        copy: {
            templates: {
                files: [
                    {
                        cwd: '<%= pkg.src %>',
                        src: '**/*.html',
                        dest: '<%= pkg.build %>/src',
                        expand: true
                    }
                ]
            },
            root: {
                files: [
                    {
                        cwd: '<%= pkg.src %>',
                        src: '*.html',
                        dest: '<%= pkg.build %>/',
                        expand: true
                    }
                ]
            }
        }
    });

    var lessConf = {
        files: [{
            expand: true,
            cwd: '<%= pkg.src %>/assets/less/',
            src: ['*.less'],
            dest: '<%= pkg.build %>/assets/css/',
            ext: '.css'
        }],
        options: {
            compress: true
        }
    };
    grunt.config.merge({
        less: {
            less_prod: r.clone(lessConf),
            less_dev:  r.assoc('options', {sourceMap: true, sourceMapFileInline: true}, lessConf)

        }
    });

}

module.exports = App;