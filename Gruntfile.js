'use strict';

module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json')
    });
    // Load all grunt tasks in /grunt-tasks folder
    grunt.loadTasks('grunt-tasks');
    // Load all grunt plugins inside devDependencies from package.json 
    require('jit-grunt')(grunt, {});

    grunt.registerTask('build', ['less:less_prod', 'copy:templates', 'copy:root']);
};