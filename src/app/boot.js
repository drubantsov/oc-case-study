'use strict';

require('angular').module('app', [
    require('./utils')
])
    .service('lookupService', require('./services/lookup-service'))
    .component('treeLookup', require('./components/tree-lookup'))
    .run(require('./run'));
