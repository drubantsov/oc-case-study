"use strict";

var LookupSearch = require('./lookup-search');

// @ngInject
function LookupService(treeLookup) {
    this.treeLookup = treeLookup;
}

LookupService.prototype = {
    searchNumber: searchNumber
};


function searchNumber(number, findFirstMatchPath) {
    var lookup = new LookupSearch(this.treeLookup);
    lookup.searchNumber(number, findFirstMatchPath);
}

function findPath(path) {
    var promise = this.treeLookup.getChildrenAsPromise('/');
    promise.then(function (childItems) {
        return childItems;
    });

}


module.exports = LookupService;