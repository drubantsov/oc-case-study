"use strict";

var QueueNode = require('../models/queue-node');

function LookupSearch(treeLookup) {
    this.treeLookup = treeLookup;
}

LookupSearch.prototype = {
    searchNumber: searchNumber,
    _lookup: _lookup
};

function searchNumber(number, callback) {
    this.queue = [];
    try {
        this._lookup(new QueueNode(), number, callback);
    } catch (ex) {
        callback({
            message: 'something went wrong',
            exception: ex
        })
    }
}

// lookup performs one request per time to TreeLookup(to follow BFS principle)
function _lookup(queueNode, findNumber, callback) {
    return this.treeLookup.getChildrenAsPromise(queueNode.path).then(function (nodes) {
        // populate queue by each level of tree from left to right BFS
        for (var index in (nodes || [])) {
            this.queue.push(new QueueNode(queueNode.path, nodes[index]));
        }
        // flush queue and return if number found
        if (nodes.indexOf(findNumber.toString()) > -1) {
            callback(null, queueNode.path);
            this.queue.length = 0;
            return;
        }
        if (this.queue.length) {
            this._lookup(this.queue.shift(), findNumber, callback);
        } else {
            callback(null, false);
        }
    }.bind(this));
}


module.exports = LookupSearch;