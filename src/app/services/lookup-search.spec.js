var LookupSearch = require('./lookup-search');

describe('lookupSearch', function () {
    var lookupSearch;

    beforeEach(function () {
        lookupSearch = new LookupSearch();
    });

    it('should not init queue after instance creation', function () {
        expect(lookupSearch.queue).toBe(undefined);
    });

    it('should init empty queue after search', function () {
        lookupSearch._lookup = jasmine.createSpy('open').and.callFake(function () {
            console.log('fake action');
        });
        lookupSearch.searchNumber(4);
        expect(lookupSearch.queue.length).toBe(0);
    });
});