"use strict";

var LookupService = require('./lookup-service'),
    utils = require('utils');

describe('lookup service tests', function () {
    var service;
    beforeEach(ModuleBuilder.forModules(utils).build());
    beforeEach(inject(function (treeLookup, $injector) {
        service = new LookupService(treeLookup);
    }));
    it('should find number 2 under the path "/"', function (done) {
        service.searchNumber(2, function (error, firstPath) {
            expect(firstPath).toBe('/');
            done();
        });
    });
    it('should find number 32 under the path "/4"', function (done) {
        service.searchNumber(32, function (error, firstPath) {
            expect(firstPath).toBe('/4');
            done();
        });
    })
});