"use strict";

function QueueNode(path, value){
    if(path === '/'){
        path = '';
    }

    this.path = (path || '') + '/' + (value || '');
}

module.exports = QueueNode;