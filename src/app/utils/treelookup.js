!function (scope, func) {
    "object" == typeof exports ? module.exports = func(require("bluebird")) : "function" == typeof define && define.amd ? define(["bluebird"], func) : scope.TreeLookup = func(scope.Promise)
}(this, function (BPromise) {
    function getTree(e) {
        return {
            1: {
                2: {
                    4: {},
                    6: {},
                    8: {},
                    10: {},
                    12: {}
                },
                3: {
                    6: {},
                    9: {},
                    12: {},
                    15: {}
                },
                4: {
                    8: {},
                    12: {},
                    16: {},
                    20: {},
                    24: {},
                    28: {}
                }
            },
            2: {
                4: {},
                8: {
                    16: {},
                    24: {}
                },
                12: {
                    24: {}
                }
            },
            3: {
                6: {
                    12: {},
                    18: {},
                    24: {},
                    30: {}
                },
                9: {
                    18: {},
                    27: {}
                },
                12: {
                    24: {},
                    36: {}
                }
            },
            4: {
                8: {},
                12: {},
                16: {
                    32: {}
                },
                20: {},
                24: {},
                28: {},
                32: {}
            },
            5: {
                10: {},
                15: {},
                20: {},
                25: {}
            },
            7: {
                14: {
                    28: {}
                },
                21: {}
            },
            11: {
                22: {},
                33: {}
            },
            13: {
                26: {}
            },
            17: {}
        }
    }

    function constructor() {
        function getKeys(path) {
            for (var pathItem, childItem, splitedPath = path.split("/"), treeRef = tree; splitedPath.length;) {
                if (pathItem = splitedPath.shift(), "" !== pathItem) {
                    if (childItem = treeRef, childItem = childItem[pathItem], void 0 === childItem) {
                        break;
                    }
                    treeRef = childItem
                }
            }

            return Object.keys(treeRef)
        }

        var tree = getTree();
        this.getChildrenAsCallback = function (key, callback) {
            setTimeout(function () {
                callback(null, getKeys(key))
            }, 0)
        }, this.getChildrenAsPromise = function (key) {
            return new BPromise(function (callback) {
                setTimeout(function () {
                    callback(getKeys(key))
                }, 0)
            })
        }
    }

    return constructor
});