/**
 * Created by dmitry on 01.06.16.
 */

var moduleName = 'app.utils';
require('angular').module(moduleName, [])
    .service('treeLookup', require('./treelookup'));

module.exports = moduleName;