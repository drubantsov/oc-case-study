var component = {
    controller: TreeLookupCtrl,
    templateUrl: __dirname + '/index.html',
    bindings: {}
};

// @ngInject
function TreeLookupCtrl(lookupService) {
    this.lookupService = lookupService;
}

TreeLookupCtrl.prototype = {
    search: search,
    change: change
};
function change() {
    this.result = null;
}

function search(number) {
    if (this.form.$invalid) {
        return;
    }
    this.loading = true;
    this.result = null;
    this.lookupService.searchNumber(number, function (error, firstPath) {
        this.result = firstPath;
        this.loading = false;
    }.bind(this));
}


module.exports = component;
