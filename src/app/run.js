
var BPromise = require("bluebird");

// @ngInject
function Run($rootScope, $window) {
    // run lifecycle for bluebirdjs Promise
    BPromise.setScheduler(function (cb) {
        $rootScope.$evalAsync(cb);
    });
}

module.exports = Run;