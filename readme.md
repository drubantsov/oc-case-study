# Implemented with Angular JS as the main framework + in conjunction with webpack also I have added a unit test.
# to compile and run app needs node, npm, webpack, grunt, karma globally installed (i used linux if no linux may be some additional stuff will be required like adding environment variables)

# dont pay attention to styles, this bunch of styles just for quick more or less nice design look

# Some quick structure details
src - sources
    app
        components - angular's directives
        models - some constructors
        services - business logic
        utils - some utils
        boot - main file
        run - config after run


# Main commands

1. npm install webpack -g  you must have webpack globally
2. npm run build - to build all stuff to ./build folder
3. cd "./build" & http-server
4. npm run test - run integration and unit tests



