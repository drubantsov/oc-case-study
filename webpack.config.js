'use strict';
var webpack = require('webpack'),
    path = require('path'),
    isProduction = process.argv.indexOf('--progress') === -1,
    plugins = [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'commons',
            filename: "commons.js",
            chunks: ['app']
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: "vendors.js",
            chunks: ['vendor', 'commons']
        }),
        new webpack.DefinePlugin({PRODUCTION: isProduction, DEBUG: !isProduction})
    ];

if (isProduction) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({sourceMap: false, compress: false}));
}

module.exports = addVendors({
    output: {
        publicPath: 'app/',
        path: path.resolve('./build/app'),
        filename: "[name].bundle.js"
    },
    entry: {
        app: './src/app/boot.js'
    },
    resolve: {
        root: __dirname,
        alias: {
            app: path.resolve('./src/app'),
            utils: path.resolve('./src/app/utils')
        },
        modulesDirectories: [
            'node_modules',
            'libs'
        ]
    },
    plugins: plugins,
    module: {
        noParse: [],
        loaders: [
            {test: /app.+\.js$/, loaders: ['ng-annotate']}
        ]
    },
    // This allows us to use relative path for html files
    node: {
        __dirname: true
    }
}, ['angular', 'bluebird']);

// Skip parsing for those packages and put them in separate file
function addVendors(config, vendors) {
    config.entry.vendor = [];
    config.module.noParse = [];
    for (var i = 0; i < vendors.length; i++) {
        config.entry.vendor.push(vendors[i]);
        config.module.noParse.push(new RegExp('^' + vendors[i] + '$'));
    }
    return config;
}