var path = require('path'),
    webpackConfig = require('../../webpack.config.js');

webpackConfig.entry = {};
webpackConfig.output = {
    publicPath: 'js/',
    filename: 'main.bundle.js'
};
webpackConfig.plugins = [];
webpackConfig.module.loaders.shift();

module.exports = webpackConfig;