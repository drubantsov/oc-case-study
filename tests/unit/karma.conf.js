var devConfig = require('./karma.dev.conf');

module.exports = function(config){
    // Load base config
    devConfig(config);

    // Override dev config
    config.set({
        singleRun: true,
        autoWatch: false,
        browsers: ['PhantomJS'],
        reporters: ['dots']
    });
};