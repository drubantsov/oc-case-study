'use strict';

var r = require('ramda'),
    webpackConf = require('./webpack');


function KarmaConfig(config) {
    config.set({
        webpack: r.clone(webpackConf),
        webpackServer: {
            stats: {
                colors: true,
                modules: false,
                chunkModules: false
            }
        },
        basePath: './',
        baseUrl: '/base/src',
        frameworks: ['jasmine'],
        files: [
            './index.js',
            '../../src/app/**/*.html'
        ],
        preprocessors: {
            './index.js': ['webpack'],
            '../../app/js/closed/**/*.html': ['ng-html2js']
        },
        reporters: ['progress', 'growl'],
        port: 9876,
        logLevel: config.LOG_INFO,
        browsers: ['Chrome'],
        singleRun: false,
        autoWatch: true,
        plugins: [
            'karma-jasmine',
            'karma-chrome-launcher',
            'karma-phantomjs-launcher',
            'karma-webpack',
            'karma-growl-reporter',
            'karma-ng-html2js-preprocessor'
        ]
    });
}


module.exports = KarmaConfig;