
Function.prototype.bind = Function.prototype.bind || function (thisp) {
        var fn = this;
        return function () {
            return fn.apply(thisp, arguments);
        };
    };

PRODUCTION = false;
require('imports?angular!angular-mocks');
require('imports?angular!ng-module-introspector/ng-module-introspector');
require('imports?angular!ng-improved-testing/ng-improved-testing');


var testsContext = require.context("app", true, /spec.js$/);
testsContext.keys().forEach(testsContext);